package com.springboot.yangspringboot;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.DeferredImportSelector;
import org.springframework.core.type.AnnotationMetadata;

import java.util.*;

public class YangAutoConfigurationImportSelector implements DeferredImportSelector {
    private ClassLoader beanClassLoader;

    //    这里面的数组就是存放Spring boot里面帮我们build好的bean对象，实例化一个bean对象放容器里面就会多一个数组元素
    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
//        如何快速查找，每个jar包下面，去解析的META-INF/下面的spring.factories文件下面的， 这就是SPI技术
//        但是这里需要注意的是：Spring 在2.7以后的spring.factories就不在推荐使用了，新的文件路径
//        META-INF/spring/org.springframework.boot.autoconfigure.AutoConfiguration.imports
//        TODO 此处的逻辑暂时还没有搞定,后期继续研究
//        List<String> configurations = new ArrayList(SpringFactoriesLoader.loadFactoryNames(this.getSpringFactoriesLoaderFactoryClass(), this.getBeanClassLoader()));
//        ImportCandidates.load(AutoConfiguration.class, this.getBeanClassLoader()).forEach(configurations::add);
//        Assert.notEmpty(configurations, "No auto configuration classes found in META-INF/spring.factories nor in META-INF/spring/org.springframework.boot.autoconfigure.AutoConfiguration.imports. If you are using a custom packaging, make sure that file is correct.");
//        Set<String> exclusions = new HashSet<>();
//        AutoConfigurationEntry autoConfigurationEntry = new AutoConfigurationEntry(configurations, exclusions);
////
//        List<String> list = new ArrayList<>();
////        List<String> list = autoConfigurationEntry.getConfigurations();
//        list.add("org.apache.catalina.startup.Tomcat");
//        String[] strings = StringUtils.toStringArray(list);
//        return strings;
        return new String[0];
    }

    protected Class<?> getSpringFactoriesLoaderFactoryClass() {
        return EnableAutoConfiguration.class;
    }

    protected ClassLoader getBeanClassLoader() {
        return this.beanClassLoader;
    }

    protected static class AutoConfigurationEntry {
        private List<String> configurations;
        private Set<String> exclusions;

        private AutoConfigurationEntry() {
            this.configurations = Collections.emptyList();
            this.exclusions = Collections.emptySet();
        }

        AutoConfigurationEntry(Collection<String> configurations, Collection<String> exclusions) {
            this.configurations = new ArrayList(configurations);
            this.exclusions = new HashSet(exclusions);
        }

        public List<String> getConfigurations() {
            return this.configurations;
        }

        public void setConfigurations(List<String> configurations) {
            this.configurations = configurations;
        }

        public Set<String> getExclusions() {
            return this.exclusions;
        }
    }

}
