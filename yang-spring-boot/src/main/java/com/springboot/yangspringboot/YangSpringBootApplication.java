package com.springboot.yangspringboot;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
//这个注解必需要加，否则不会扫描对的文件夹下面
@ComponentScan
@SpringBootConfiguration
//这个注解是必须要的，否则我们读不到自己手写的Spring jar的文件
@Import(WebServerAutoConfiguration.class)
public @interface YangSpringBootApplication {
}
