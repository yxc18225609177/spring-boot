package com.springboot.yangspringboot;

import org.springframework.web.context.WebApplicationContext;

public interface WebServer {
    void start(WebApplicationContext applicationContext);
}
