package com.springboot.yangspringboot;

import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Conditional(YangCondition.class)
public @interface YangConditionalOnClass {
    String value();
}
