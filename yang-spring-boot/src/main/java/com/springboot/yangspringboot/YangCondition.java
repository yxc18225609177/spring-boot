package com.springboot.yangspringboot;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.util.MultiValueMap;

import java.util.Map;

public class YangCondition implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
//        加载类，看是否存在
        try {
//            这个地方就是拿前面的条件注解传递的参数
            Map<String, Object> annotationAttributes = metadata.getAnnotationAttributes(YangConditionalOnClass.class.getName());
//            这里是的value就是之前注解里面的变量value
            String value = (String) annotationAttributes.get("value");
            context.getClassLoader().loadClass(value);
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }
}
