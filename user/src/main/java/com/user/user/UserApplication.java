package com.user.user;

import com.springboot.yangspringboot.YangSpringApplication;
import com.springboot.yangspringboot.YangSpringBootApplication;

@YangSpringBootApplication
public class UserApplication {

    public static void main(String[] args) {
        YangSpringApplication.run(UserApplication.class);
    }

}
